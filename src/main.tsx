import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './app/less/index.less';

//import { Game } from 'app/components/Game';
import GameRoot from 'app/components/GameRoot';

ReactDOM.render(<GameRoot />, document.getElementById('root'));