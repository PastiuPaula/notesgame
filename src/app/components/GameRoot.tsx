import * as React from "react";
import LoadingScreen from "./LoadingScreen";
import SettingsScreen from "./SettingsScreen";
import PlayersScreen from "./PlayersScreen";
import WordsScreen from "./WordsScreen";
import TeamScreen from "./TeamScreen";
import GameScreen from "./GameScreen";

interface Props{

}

interface State{
    gameState:'loadingScreen' | 'settingsScreen' | 'playersScreen' | 'wordsScreen' | 'teamScreen' | 'gameScreen'
}

export default class GameRoot extends React.Component<Props, State>{
    constructor(props: Props){
        super(props);
        this.state = {
            gameState : 'loadingScreen'
        };
    }

    render(){
        switch (this.state.gameState){
            case 'loadingScreen' : return(
                <LoadingScreen onGameStart={() => this.setState({gameState : 'settingsScreen'})} />
            )
            case 'settingsScreen' : return(
                <SettingsScreen onContinue={() => this.setState({gameState : 'playersScreen'})}/>
            )
            case 'playersScreen' : return (
                <PlayersScreen onContinue={() => this.setState({gameState : 'wordsScreen'})}/>
            )
            case 'wordsScreen' : return (
                <WordsScreen onContinue={() => this.setState({gameState : 'teamScreen'})}/>
            )
            case 'teamScreen' : return (
                <TeamScreen onContinue={() => this.setState({gameState : 'gameScreen'})}/>
            )
            case 'gameScreen' : return (
                <GameScreen onContinue={() => this.setState({gameState : 'gameScreen'})}/>
            )
        }

        return null;
    }
}