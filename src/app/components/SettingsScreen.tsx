import * as React from "react";

interface Props {
    onContinue: () => void;
}

interface State {
    wordsPerPlayer: number;
    timePerPlayerSec: number;
}

export default class SettingsScreen extends React.Component<Props, State>{
    constructor(props: Props) {
        super(props);
        this.state = {
            wordsPerPlayer: 3,
            timePerPlayerSec: 60
        }
    };

    incrementWordsPerPlayer() {
        this.setState((prevState) => {
            return {
                wordsPerPlayer: prevState.wordsPerPlayer + 1
            }
        });
    }

    decrementWordsPerPlayer() {
        this.setState((prevState) => {
            return {
                wordsPerPlayer: (prevState.wordsPerPlayer > 1) ? prevState.wordsPerPlayer - 1 : prevState.wordsPerPlayer
            }
        });
    }

    incrementTimePerPlayerSec() {
        this.setState((prevState) => {
            return {
                timePerPlayerSec: prevState.timePerPlayerSec + 10
            }
        });
    }

    decrementTimePerPlayerSec() {
        this.setState((prevState) => {
            return {
                timePerPlayerSec: (prevState.timePerPlayerSec > 30) ? prevState.timePerPlayerSec - 10 : prevState.timePerPlayerSec
            }
        });
    }

    render() {
        return (
            <div className="general_view">
                <div>
                    <div>
                        <span>How many words per player?</span>
                    </div>
                    <div>
                        <button className="increment_decrement_button dec" onClick={() => this.decrementWordsPerPlayer()}>-</button>
                        <span>{this.state.wordsPerPlayer}</span>
                        <button className="increment_decrement_button inc" onClick={() => this.incrementWordsPerPlayer()}>+</button>
                    </div>
                </div>
                <br />
                <div>
                    <div>
                        <span>How much time per player?</span>
                    </div>
                    <div>
                        <button className="increment_decrement_button dec" onClick={() => this.decrementTimePerPlayerSec()}>-</button>
                        <span>{this.state.timePerPlayerSec}</span>
                        <button className="increment_decrement_button inc" onClick={() => this.incrementTimePerPlayerSec()}>+</button>
                    </div>
                </div>
                <br />
                <button className="continue_button" onClick={() => this.props.onContinue()}>Continue</button>
            </div>
        )
    }
}