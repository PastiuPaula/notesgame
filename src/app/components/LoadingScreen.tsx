import * as React from "react";
import Logo from "app/components/Logo";

interface Props {
    onGameStart: () => void;
}

export default class LoadingScreen extends React.Component<Props>{
    render() {
        return (
            <div className="general_view vertical_alignment">
                <div>
                    <Logo />
                    <button className="start_game_button bounce-animation" onClick={() => this.props.onGameStart()}>Start Game</button>
                </div>
            </div>
        )
    }
}