import * as React from "react";

const PlayerList = () => {
    const list = ['player1', 'player2']
    return (
        <div>
            {list.map((elem, i) => {
                <div>{list[i]}</div>
            })}
        </div>

    )
}


class WordList extends React.Component {
    list: string[] = []

    addWord(word: string) {
        this.list.push(word);
    }

    render() {
        return (
            <div>
                ...
                {this.list.map((elem, i) => {
                    <div>{this.list[i]}</div>
                })}
            </div>

        )
    }
}

export class Game extends React.Component {
    state = {
        numberOfPlayers: 0,
        numberOfWordsPerPlayer: 0,
        players: PlayerList,
        words: WordList,
        vizibility: true
    };

    changeNumberOfPlayers(newNumberOfPlayers: number) {
        this.setState({
            numberOfPlayers: newNumberOfPlayers
        });
    }

    changeNumberOfWordsPerPlayer(newNumberOfWordsPerPlayer: number) {
        this.setState({
            numberOfWordsPerPlayer: newNumberOfWordsPerPlayer
        });
    }

    startGame(event: React.FormEvent) {
        event.preventDefault();
        this.setState({ vizibility: false });
        console.log("submited");
    }

    render() {
        return (
            <div id="root">
                <div style={{ visibility: this.state.vizibility ? 'visible' : 'hidden' }}>
                    <form onSubmit={(e) => { this.startGame(e); }}>
                        <div>
                            <h1>Notes Game</h1>
                            <button type="submit">Start Game </button>
                        </div>
                    </form>
                </div>
                <WordList />
            </div>
        )
    }
}