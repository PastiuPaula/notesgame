import * as React from "react";

interface Props {
    onContinue: () => void;
    //players: string[];
}

interface State {
    currentInputPlayer: '';
}

export default class PlayersScreen extends React.Component<Props, State>{
    constructor(props: Props, state: State) {
        super(props);
        this.state = state;
    }

    players: string[] = [];
    currentPlayerName: string = "";

    addPlayer() {
        console.log(this.currentPlayerName);        
        this.players.push(
            
        );

    }

    handleInputChange(){
        this.setState({})
    }

    render() {
        return (
            <div>
                <div>
                    <div>Add players name:</div>
                    <input type="text" placeholder="ex. David" onChange={(e) => this.state.currentInputPlayer}></input>
                    <button onClick={()=>{this.addPlayer()}}>Add Player</button>
                </div>
                <div>
                    {this.players.map((elem, i) => 
                        <div>{this.players[i]}</div>
                    )}
                </div>
                <div>
                    <button onClick={() => this.props.onContinue()}>Continue</button>
                    </div>
            </div>
        )
    }
}