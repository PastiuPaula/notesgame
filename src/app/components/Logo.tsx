import * as React from "react";

export default class Logo extends React.Component {
    render() {
        return (
            <div className="logo">
                <img src="assets/time.svg" className="logo_time"/>
                <img src="assets/note.svg" className="logo_note"/>
                <img src="assets/mime.svg" className="logo_mime"/>
            </div>
        );
    }
}